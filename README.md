<br />
<br />
<br />
<p align="center">
    <img src="https://images.gitee.com/uploads/images/2020/0717/121913_a0197826_7825244.png"  width="500px" height="101px"  alt="LOGO">
</p>


<h1 align="center">
<a id="千鸟云商B2B订货系统 - 国内首款免费进销存ERP B2B商城系统" class="anchor"></a> 千鸟云商B2B订货系统 - 国内首款免费进销存ERP B2B商城系统</h1>


## 千鸟云商免费版，8月面世！
----

## 介绍
#### 千鸟云商 - 为传统商户量身打造的全渠道产业互联线上线下一体化新零售营销系统! 以客户为中心，将线上和线下、进销存、CRM、财务一体化，真正的做到了将客户、财务、业务完美融合，支持连锁、代理、经销多种运营模式，数据权限精细到员工、店铺、组织，完美适配各种管理方案！
#### 小程序一键发布，后台自定义APP界面模板，多套模板可供选择；强大的后台管理及简单的操作流程，让您一见倾心。带有国际范儿的前后台界面样式，让您一见舒心。严谨的功能结合、贴心的提示提醒、全面的学习教程，让您一用放心。

----
## 核心功能

#### ERP、进销存、B2B商城、订货商城、分销、库存管理、财务管理、阶梯定价、客户类型定价、自定义UI、小程序、H5

#### 订货系统源码、新零售系统源码、仿有赞零售源码、仿易订货源码，仿订货宝源码。支持批发，支持零售，支持连锁 N种运营方式。

----
## 相关网址
<p><a href="">前端下载(等待添加)</a>
| <a href="https://www.kancloud.cn/qianniaoyunshang/houtaishiyongliucheng/1806003">使用手册</a>
| <a href="">安装说明(等待添加)</a>
| <a href="https://www.qianniaovip.com">官网地址</a>



----

#### **微信公众号二维码&微信小程序演示&微信手机后台管理**

<img src="https://images.gitee.com/uploads/images/2020/0717/122722_5f01f157_7825244.png"  width="258px" height="320px"  alt="真棒" align=center />

<img  src="https://images.gitee.com/uploads/images/2020/0717/122732_1bc1afbb_7825244.png"  width="258px" height="320px"  alt="真棒" align=center />

<img  src="https://images.gitee.com/uploads/images/2020/0717/122742_12c0235f_7825244.png"  width="258px" height="320px" alt="真棒" align=center />


----

### **系统演示**

#### **千鸟云商：**[http://www.qianniaovip.com](http://www.qianniaovip.com/)

#### **体验地址：**[http://www.qianniao.vip](http://www.qianniao.vip/)

#### **体验账号：** 18888888888

#### **体验密码：** 123456

----

## :tw-1f50a:  开源版使用须知
#### 1.允许用于个人学习、毕业设计、教学案例、公益事业;

#### 2.如果商用必须保留版权信息，请自觉遵守;

#### 3.禁止将本项目的代码和资源进行任何形式的出售，产生的一切任何后果责任由侵权者自负。

----

### **免费版功能表**

![输入图片说明](https://images.gitee.com/uploads/images/2020/0717/144737_5eee125f_7825244.png "微信图片_20200717144645.png")

----

### **小程序前台截图**

----

### **系统后台截图**
